proftpd-mod-fsync (0.3-4) unstable; urgency=medium

  [ Hilmar Preusse ]
  * Remove Depend on "proftpd-basic (>= 1.3.3d-4~)".
  * Add file debian/gitlab-ci.yml for Salsa-CI.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on proftpd-dev.

 -- Hilmar Preusse <hille42@web.de>  Thu, 22 Jun 2023 07:42:04 +0200

proftpd-mod-fsync (0.3-3) unstable; urgency=medium

  [ Francesco Paolo Lovergine ]
  * Do not ship .a and .la files in final package.

  * Lintian
    - package-uses-deprecated-debhelper-compat-version 9
    - unstripped-static-library
    - trailing-whitespace
    - debian-watch-uses-insecure-uri
    - Standards version 4.5.0, no changes needed.
    - silent-on-rules-requiring-root
    - unversioned-copyright-format-uri

 -- Hilmar Preusse <hille42@web.de>  Sat, 22 Aug 2020 23:50:27 +0200

proftpd-mod-fsync (0.3-2) unstable; urgency=medium

  * Policy bumped to 4.1.4. No changes required.
  * Vcs-* updated for salsa.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Tue, 01 May 2018 16:25:12 +0200

proftpd-mod-fsync (0.3-1) unstable; urgency=medium

  [ Fabrizio Regalli ]

  * Removing libacl1-dev as BD

  [ Hilmar Preuße ]

  * New upstream release: can be built with ProFTP 1.3.6 (Closes: #892373)
    * Bump BD version of proftp-dev to >= 1.3.6.
  * Overhaul clean target in debian/rules to make it work
  * Remove "DM-Upload-Allowed" field.
  * Standards Version 4.1.3, no changes needed.
  * debhelper compat 9, BD Version of debhelper adapted.
  * Remove Fabrizio Regalli from Uploaders field.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Wed, 14 Mar 2018 12:31:36 +0100

proftpd-mod-fsync (0.2-1) unstable; urgency=low

  [ Fabrizio Regalli ]
  * Initial Release. (Closes: #623228)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Tue, 11 Oct 2011 12:29:18 +0200
